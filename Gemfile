source 'https://rubygems.org'

git_source(:gitlab) do |repo_name|
  "https://gitlab.com/#{repo_name}.git"
end

ruby '> 2.6.0'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '6.0.3.2'
# Use postgresql as the database for Active Record
gem 'sqlite3'
# Use SCSS for stylesheets
gem 'sassc-rails', '~> 2.0'

gem 'bootsnap'

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'jquery-turbolinks'
gem 'turbolinks'

gem 'bootstrap-sass'
gem 'font-awesome-rails'
gem 'sprockets', '< 4'

gem 'fastercsv'

gem 'faker'

gem 'annotate'

gem 'awesome_nested_set'
gem 'enumerize'

gem 'haml'

gem 'concurrent-ruby'

gem 'http_accept_language'
gem 'i18n-complements'
gem 'rails-i18n'
gem 'routing-filter'

gem 'nokogiri', '~> 1.10'


# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.0'

# Use ActiveModel has_secure_password
# gem 'bcrypt', '~> 3.1.7'

# Use Puma as the app server
gem 'puma'

gem 'onoma', gitlab: 'ekylibre/onoma', tag: 'v0.5.0'

gem 'elastic-apm', '~> 3.4.0'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

group :development, :test do
  # gem 'quiet_assets'
  # gem 'rack-mini-profiler'

  # Code metrics
  # gem 'rails_best_practices', require: false
  # gem 'rubocop', require: false
  # Use Uglifier as compressor for JavaScript assets
  gem 'uglifier', '>= 1.3.0'

  # Webservers
  gem 'thin'

  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'

  gem 'listen'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 4.0'
end