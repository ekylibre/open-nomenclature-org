# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_07_20_094213) do

  create_table "item_translations", force: :cascade do |t|
    t.integer "item_id", null: false
    t.string "language", null: false
    t.text "label"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["language"], name: "index_item_translations_on_language"
  end

  create_table "items", force: :cascade do |t|
    t.integer "nomenclature_id", null: false
    t.integer "parent_id"
    t.string "parent_name"
    t.string "name", null: false
    t.string "state", null: false
    t.integer "lft"
    t.integer "rgt"
    t.integer "depth"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name"], name: "index_items_on_name"
    t.index ["nomenclature_id"], name: "index_items_on_nomenclature_id"
    t.index ["parent_id"], name: "index_items_on_parent_id"
    t.index ["parent_name"], name: "index_items_on_parent_name"
  end

  create_table "nomenclature_translations", force: :cascade do |t|
    t.integer "nomenclature_id", null: false
    t.string "language", null: false
    t.text "label"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["language"], name: "index_nomenclature_translations_on_language"
  end

  create_table "nomenclatures", force: :cascade do |t|
    t.string "name", null: false
    t.boolean "translateable", default: false, null: false
    t.boolean "hierarchical", default: false, null: false
    t.string "state", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["name"], name: "index_nomenclatures_on_name"
  end

  create_table "properties", force: :cascade do |t|
    t.integer "item_id", null: false
    t.integer "nature_id", null: false
    t.text "value", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["item_id"], name: "index_properties_on_item_id"
    t.index ["nature_id"], name: "index_properties_on_nature_id"
  end

  create_table "property_nature_translations", force: :cascade do |t|
    t.integer "property_nature_id", null: false
    t.string "language", null: false
    t.text "label"
    t.text "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["language"], name: "index_property_nature_translations_on_language"
  end

  create_table "property_natures", force: :cascade do |t|
    t.integer "nomenclature_id", null: false
    t.string "name", null: false
    t.string "datatype", null: false
    t.boolean "required", default: false, null: false
    t.text "default_value"
    t.string "fallbacks"
    t.string "state", null: false
    t.integer "choices_nomenclature_id"
    t.string "choices_nomenclature_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["choices_nomenclature_id"], name: "index_property_natures_on_choices_nomenclature_id"
    t.index ["choices_nomenclature_name"], name: "index_property_natures_on_choices_nomenclature_name"
    t.index ["name"], name: "index_property_natures_on_name"
    t.index ["nomenclature_id"], name: "index_property_natures_on_nomenclature_id"
  end

end
