class FixSchema < ActiveRecord::Migration[6.0]
  def change
    remove_column :nomenclatures, :nomenspace_id, :integer
    remove_column :nomenclatures, :property_id, :integer

    change_column_null :property_natures, :choices_nomenclature_id, true
    change_column_null :items, :parent_id, true

    drop_table :nomenspace_translations
    drop_table :nomenspaces
  end
end
