FROM ruby:2.7-alpine AS build-env

ENV BUILD_PACKAGES="build-base" \
    DEV_PACKAGES="zlib-dev nodejs yarn sqlite-dev git" \
    RUBY_PACKAGES="tzdata" \
    RAILS_ENV=production \
    NODE_ENV=production \
    BUNDLE_APP_CONFIG="/app/.bundle"

WORKDIR /app
# install packages
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $BUILD_PACKAGES $DEV_PACKAGES $RUBY_PACKAGES
COPY Gemfile* ./
# install rubygem
RUN bundle config --global frozen 1 \
    && bundle config set without 'development:test:assets' \
    && bundle install -j $(nproc) --retry 3 --path=vendor/bundle \
    # Remove unneeded files (cached *.gem, *.o, *.c)
    && rm -rf vendor/bundle/ruby/2.7.0/cache/*.gem \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.c" -delete \
    && find vendor/bundle/ruby/2.7.0/gems/ -name "*.o" -delete
RUN yarn install --production
COPY . .
#RUN bin/rails webpacker:compile
RUN cp config/database-sample.yml config/database.yml &&\
    bin/rake assets:precompile &&\
    bin/rake db:migrate 2>/dev/null &&\
    bin/rake db:seeds 2>/dev/null || DISABLE_DATABASE_ENVIRONMENT_CHECK=1 bin/rake db:setup
# Remove folders not needed in resulting image
RUN rm -rf node_modules tmp/cache app/assets vendor/assets spec

############### Build step done ###############
FROM ruby:2.7-alpine

ENV PACKAGES="tzdata sqlite-libs" \
    RAILS_ENV=production \
    BUNDLE_APP_CONFIG="/app/.bundle"

WORKDIR /app
# install packages
RUN apk update \
    && apk upgrade \
    && apk add --update --no-cache $PACKAGES
COPY --from=build-env /app /app

# Expose Puma port
EXPOSE 3000

# Start up
CMD ["docker/startup.sh"]
