/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
$(document).on('click contextmenu', '*[data-toggle="tree"]', function(event) {
  const button = $(this);
  const item = button.closest('.item');
  $.collapse(item, !item.hasClass('collapsed'), event.type === 'contextmenu');
  return false;
});

$.collapse = function(item, collapse, deep) {
  if (deep == null) { deep = false; }
  if (collapse) {
    item.addClass('collapsed');
  } else {
    item.removeClass('collapsed');
  }
  const list = item.find('> ul.item-list');
  if (collapse) {
    list.slideUp();
  } else {
    list.slideDown();
  }
  if (deep) {
    return list.find('> li.item').each(function() {
      return $.collapse($(this), collapse, true);
    });
  }
};
  
